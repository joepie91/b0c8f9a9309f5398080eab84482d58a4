const Promise = require("bluebird");

function makeRequest(url) {
  return Promise.try(() => {
    return bhttp.get(url); // Whether an error occurs here...
  }).then((response) => {
    return response.body;  // ... or here...
  }).catch({name: "TypeError"}, (err) => { // ... and as long as it's not a TypeError in the previous two places...
    console.log("A wild TypeError appears!", err);
    process.exit(1);
  });
}

function makeManyRequests(urls) {
  return Promise.try(() => {
    if (urls.length === 0) {
      throw new Error("Must specify at least one URL"); // ... or here (even if it's a TypeError!) ...
    } else {
      return urls;
    }
  }).map((url) => {
    return makeRequest(url);
  });
}

function doMyThing() {
  return Promise.try(() => {
    return makeManyRequests([
      "http://google.com/",
      "http://bing.com/",
      "http://yahoo.com"
    ]);
  }).catch((err) => {
    console.log("Oh noes!", err); // ... it will always end up here, as long as it wasn't caught by our previous .catch.
  });
}